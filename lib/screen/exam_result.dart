// sınav sonu karnesi
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dhbtokulu/constants/color.dart';
import 'package:dhbtokulu/model/auth_user_values_provider.dart';
// import 'package:dhbtokulu/model/duyuru_model.dart';
import 'package:dhbtokulu/model/trial_exam_provider.dart';
import 'package:dhbtokulu/screen/exam_answer_key.dart';
// import 'package:dhbtokulu/screen/notification_page.dart';
import 'package:dhbtokulu/services/firebase_database.dart';
import 'package:dhbtokulu/widgets/exam_result_container.dart';
import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:hive/hive.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';

class ExamResult extends StatefulWidget {
  ExamResult({Key? key}) : super(key: key);

  @override
  State<ExamResult> createState() => _ExamResultState();
}

final _firestore = FirebaseFirestore.instance;
late List<DocumentReference> examQueList = [];
late List<int> falseQueList = [];
late List<int> trueQueList = [];
late List<int> blankQueList = [];
List<String> cevapAnahtari = [
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
];
List<String> kisiCevap = [
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
];

class _ExamResultState extends State<ExamResult> {
  int dogruCevapSayisi = 0, yanlisCevapSayisi = 0, bosCevapSayisi = 0;
  bool ilkGosterim = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    getDelay();
    getExamAnswerKeys();
    getUserAnswers();
  }

  @override
  Widget build(BuildContext context) {
    dogruCevapSayisi = 0;
    yanlisCevapSayisi = 0;
    bosCevapSayisi = 0;
    falseQueList = [];
    trueQueList = [];
    blankQueList = [];

    calcExamResult();

    return bodyScaffold(context);
  }

  Scaffold bodyScaffold(BuildContext context) {
    // final listenDocument =
    //     Provider.of<FirebaseDatabaseService>(context, listen: false);
    return Scaffold(
      appBar: AppBar(
        title: Column(
          children: [
            Text(
              "Merhaba",
              style: TextStyle(color: FixedColor().color2),
            ),
            Text(
                Hive.box("logindata").get("isim") == null
                    ? ""
                    : "${Hive.box("logindata").get("isim").toString().toUpperCase()} ${Hive.box("logindata").get("soyisim").toString().toUpperCase()}",
                style: TextStyle(color: FixedColor().color3))
          ],
        ),
        // actions: [
        //   Padding(
        //       padding: const EdgeInsets.only(right: 5),
        //       child: StreamBuilder<List<dynamic>>(
        //           stream: listenDocument.duyuruStream(),
        //           builder: (BuildContext context,
        //               AsyncSnapshot<List<dynamic>> snapshot) {
        //             if (snapshot.hasError) {
        //               return const Icon(
        //                 Icons.notifications_none,
        //                 size: 40,
        //               );
        //             }

        //             if (snapshot.connectionState == ConnectionState.waiting) {
        //               return const Icon(
        //                 Icons.notifications_none,
        //                 size: 40,
        //               );
        //             }
        //             List<DuyuruModel>? docSnapList =
        //                 snapshot.data!.cast<DuyuruModel>();
        //             List? duyurular = Hive.box("logindata").get("duyurular");
        //             List duyurular1 = [];
        //             Function eq = const ListEquality().equals;
        //             bool equals = true;
        //             for (var i in docSnapList) {
        //               duyurular1.add(i.id);
        //             }
        //             if (eq(duyurular1, duyurular)) {
        //               equals = true;
        //             } else if (duyurular!.length > duyurular1.length) {
        //               equals = true;
        //             } else {
        //               equals = false;
        //             }
        //             return IconButton(
        //               onPressed: () {
        //                 Navigator.push(
        //                     context,
        //                     PageTransition(
        //                         type: PageTransitionType.rightToLeft,
        //                         duration: const Duration(milliseconds: 500),
        //                         child: NotificationPage(
        //                           docSnapList: docSnapList,
        //                           duyurular1: duyurular1,
        //                         )));
        //               },
        //               icon: const Icon(
        //                 Icons.notifications_none,
        //                 size: 40,
        //               ),
        //               color: equals ? FixedColor().color2 : Colors.red,
        //             );
        //           }))
        // ],
        centerTitle: true,
        backgroundColor: FixedColor().color1,
        shadowColor: FixedColor().color1,
        iconTheme: IconThemeData(
          color: FixedColor().color3,
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Center(
                child: Container(
              child: const Center(
                  child: Text(
                "SINAV SONUÇ KARNESİ",
                style: TextStyle(fontSize: 20),
              )),
              width: MediaQuery.of(context).size.width * 0.9,
              height: MediaQuery.of(context).size.height * 0.08,
              margin: const EdgeInsets.all(8),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                color: FixedColor().color2,
              ),
            )),
            ElevatedButton(
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) => ExamAnswerKey(
                              blankQueList: blankQueList,
                              falseQueList: falseQueList,
                              trueQueList: trueQueList,
                              bosCevapSayisi: bosCevapSayisi,
                              yanlisCevapSayisi: yanlisCevapSayisi,
                              dogruCevapSayisi: dogruCevapSayisi,
                            )));
              },
              child: ListTile(
                title: Text(
                  "CEVAP ANAHTARINI GÖRÜNTÜLE",
                  textAlign: TextAlign.center,
                  style: TextStyle(color: FixedColor().color1, fontSize: 14),
                ),
                leading: Icon(
                  Icons.assignment_turned_in_outlined,
                  color: FixedColor().color1,
                  size: 40,
                ),
              ),
              style: ElevatedButton.styleFrom(
                fixedSize: Size(MediaQuery.of(context).size.width * 0.9,
                    MediaQuery.of(context).size.height * 0.08),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20)),
              ),
            ),
            ExamResultContainer(
              dogruCevapSayisi: dogruCevapSayisi,
              yanlisCevapSayisi: yanlisCevapSayisi,
              bosCevapSayisi: bosCevapSayisi,
              text: "${choiceNotify.value.examName.toUpperCase()}",
            ),
          ],
        ),
      ),
    );
  }

  void setExamSnapList() {
    DocumentReference denemeRef =
        _firestore.collection("deneme").doc(choiceNotify.value.examName);
    denemeRef
      ..get().then((DocumentSnapshot docSnap) {
        //trueQueList = docSnap;
      });
  }

  void getExamAnswerKeys() {
    DocumentReference denemeRef =
        _firestore.collection("deneme").doc(choiceNotify.value.examName);
    examQueList.add(denemeRef);
    print("---------------examQueList1:${examQueList.length}");
    denemeRef
      ..get().then((DocumentSnapshot docSnap) {
        for (int i = 0; i < 40; i++) {
          cevapAnahtari[i] = docSnap.get('cevapAnahtari')[i].toString();
        }
      });
    authNotify.value.cevapAnahtari = cevapAnahtari;
  }

  getUserAnswers() {
    DocumentReference userAnswerRef = _firestore
        .collection("kullanici")
        .doc(Hive.box("logindata").get("useruid"))
        .collection("cozulenSinavlar")
        .doc(choiceNotify.value.examName);
    userAnswerRef
      ..get().then((DocumentSnapshot docSnap) {
        for (int i = 0; i < docSnap.get('cevaplar').length; i++) {
          kisiCevap[i] = docSnap.get('cevaplar')[i].toString();
        }
        setState(() {
          kisiCevap = kisiCevap;
        });
      });
    authNotify.value.kisiCevap = kisiCevap;
  }

  void getDelay() {
    const CircularProgressIndicator();
    Future.delayed(const Duration(seconds: 5)); // Duration to wait
  }

  void calcExamResult() {
    // print("------------------------------>>>>>>>>ExamQues>>>${examQueList[0].data()}");
    for (int i = 0; i < cevapAnahtari.length; i++) {
      if (cevapAnahtari[i] == kisiCevap[i]) {
        dogruCevapSayisi++;
        trueQueList.add(i);
        blankQueList.add(100);
        falseQueList.add(100);
        //////////////////////////---------------------------------------------------------------------trueQueList.add(examQueList[i]);
      } else if (cevapAnahtari[i] != kisiCevap[i]) {
        if (kisiCevap[i] == " ") {
          bosCevapSayisi++;
          blankQueList.add(i);
          trueQueList.add(100);
          falseQueList.add(100);
          // blankQueList.add(examQueList[i]);
        } else {
          yanlisCevapSayisi++;
          falseQueList.add(i);
          blankQueList.add(100);
          trueQueList.add(100);
          // falseQueList.add(examQueList[i]);
        }
      }
    }
  }
}
