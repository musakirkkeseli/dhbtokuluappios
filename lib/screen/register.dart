import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:url_launcher/url_launcher.dart';

import '../constants/color.dart';
import '../model/auth_user_values_provider.dart';
import '../widgets/custom_dialog_box.dart';
import '../widgets/register_text_form_field.dart';
import 'login.dart';

class Register extends StatefulWidget {
  const Register({Key? key}) : super(key: key);

  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  final _formKey = GlobalKey<FormState>();
  bool _kvkk = false;
  bool _isSelectionKvkk = true;
  bool _sms = false;
  final String _url = "https://www.dhbtokulu.com/kisisel-verilerin-korunmasi";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Üye Ol",
          style: TextStyle(color: FixedColor().color3),
        ),
        centerTitle: true,
        backgroundColor: FixedColor().color1,
        iconTheme: IconThemeData(
          color: FixedColor().color3,
        ),
        shadowColor: FixedColor().color1,
      ),
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              RegisterTextFormField(),
              CheckboxListTile(
                onChanged: (bool? value) {
                  setState(() {
                    _kvkk = value!;
                    if (value == true) {
                      _isSelectionKvkk = true;
                    }
                  });
                },
                value: _kvkk,
                controlAffinity: ListTileControlAffinity.leading,
                title: Text(
                  "KVKK Metnini Okudum ve Onaylıyorum",
                  style: TextStyle(
                      color: _isSelectionKvkk == false
                          ? Colors.red
                          : FixedColor().color2),
                ),
                activeColor: FixedColor().color3,
              ),
              TextButton(
                child: const Text("KVKK metni için dokunun."),
                onPressed: () {
                  _launchURL();
                },
              ),
              // CheckboxListTile(
              //   onChanged: (bool? value) {
              //     setState(() {
              //       _sms = value!;
              //     });
              //   },
              //   value: _sms,
              //   controlAffinity: ListTileControlAffinity.leading,
              //   title: Text(
              //     "Bilgilendirme ve Kampanyalardan SMS'le haberdar olmak istiyorum.",
              //     style: TextStyle(color: FixedColor().color2),
              //   ),
              //   activeColor: FixedColor().color3,
              // ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 20),
                child: ElevatedButton(
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      _formKey.currentState!.save();
                      if (_kvkk == false) {
                        setState(() {
                          _isSelectionKvkk = false;
                        });
                      } else {
                        registerFirebaseAuth(_sms);
                      }
                    }
                  },
                  child: const Text("Kayıt Ol", style: TextStyle(fontSize: 17)),
                  style: ElevatedButton.styleFrom(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(50)),
                    primary: FixedColor().color3,
                    fixedSize: Size(MediaQuery.of(context).size.width * 0.9,
                        MediaQuery.of(context).size.height * 0.055),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _launchURL() async => await launch(_url);

  Future<void> registerFirebaseAuth(bool sms) async {
    try {
      await FirebaseAuth.instance
          .createUserWithEmailAndPassword(
              email: authNotify.value.eposta, password: authNotify.value.sifre)
          .then((kullanici) {
        FirebaseFirestore.instance
            .collection("kullanici")
            .doc(kullanici.user!.uid)
            .set({
          "eposta": authNotify.value.eposta,
          "gsm": "",
          "isim": authNotify.value.isim,
          "sifre": authNotify.value.sifre,
          "soyisim": authNotify.value.soyisim,
          "sms": false
        }).then((value) {
          // showAlertDialog(context);
          Hive.box("logindata").putAll({
            "useruid": kullanici.user!.uid,
            "isim": authNotify.value.isim,
            "soyisim": authNotify.value.soyisim
          });
          Navigator.of(context).pushNamedAndRemoveUntil(
              "/trialexam", ModalRoute.withName("/navbar"));
        });
      });
    } on FirebaseAuthException catch (e) {
      // print('--------------->>>>Failed with error code: ${e.code}');
      // print("--------------->>>>${e.message}");
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return CustomDialogBox(
              title: "Kayıt Hatası",
              descriptions:
                  "Dhbt Okulu'na Kayıt Sırasında Hata Meydana Geldi, Lütfen Tüm Bilgileri Doğru Şekilde Giriniz!",
              text: "Tekrar Dene",
            );
          });
    }
  }

  showAlertDialog(BuildContext context) {
    return showDialog(
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text(
                "Kayıt işleminiz başarıyla gerçekleştirilmiştir. Tamam butonuna dokunarak Giriş safasına gidebilirsiniz."),
            actions: [
              TextButton(
                  onPressed: () {
                    Navigator.pop(context);
                    Navigator.push(context,
                        MaterialPageRoute(builder: (_) => const Login()));
                    FirebaseFirestore.instance
                        .collection("kullanici")
                        .doc(authNotify.value.eposta)
                        .collection("cozulenSinavlar");
                  },
                  child: const Text("Tamam"))
            ],
          );
        },
        context: context);
  }
}
