// sınav sorularının gösterildiği sayfa
import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dhbtokulu/constants/color.dart';
import 'package:dhbtokulu/model/trial_exam_provider.dart';
import 'package:dhbtokulu/screen/exam_result.dart';
import 'package:dhbtokulu/widgets/exam_ques_container.dart';
import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';

class TrialExamQues extends StatefulWidget {
  final String whichTrialExam;
  const TrialExamQues({
    Key? key,
    required this.whichTrialExam,
  }) : super(key: key);

  @override
  _TrialExamQuesState createState() => _TrialExamQuesState();
}

class _TrialExamQuesState extends State<TrialExamQues> {
  bool isStart = false;
  bool finish = false;
  int milisaniye = 0;
  int saniye = 0;
  int dakika = 60;
  late Timer _timer;
  final _firestore = FirebaseFirestore.instance;
  void _start() {
    setState(() {
      isStart = !isStart;
      if (isStart) {
        const onesec = Duration(seconds: 1);
        _timer = Timer.periodic(onesec, (timer) {
          setState(() {
            if (dakika == 60) {
              dakika = dakika - 1;
              saniye = 59;
            } else {
              saniye = saniye - 1;
              if ((saniye == 0) & (dakika == 0)) {
                _timer.cancel();
              } else if ((saniye == 0) & (dakika != 0)) {
                dakika = dakika - 1;
                saniye = 59;
              }
            }

            if (dakika == 0 && saniye == 0) {
              ///////////////7-------------------------------------->>>>>>>>>>>>>>>< kişinin sınav bilgilerinin fb'ye yazıldığı yer
              addFbUserAnswers();
              gotoExamResult();
              choiceNotify.clearChoice();
            }
          });
        });
      } else {
        _timer.cancel();
      }
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    if (isStart) {
      _timer.cancel();
    }
    super.dispose();
  }

  //////-------------> sayfa yüklenince daha önceden işareyler varsa sil
  @override
  void initState() {
    super.initState();
    choiceNotify.clearChoice();
    _start();
  }

  @override
  Widget build(BuildContext context) {
    choiceNotify.value.examName = widget.whichTrialExam;

    Stream<QuerySnapshot> examRef = _firestore
        .collection("deneme")
        .doc(widget.whichTrialExam)
        .collection("sorular")
        .orderBy('soruNum', descending: false)
        .snapshots();

    return Scaffold(
      appBar: AppBar(
        title: Text(
          "${(dakika ~/ 10)}${(dakika % 10)}:${(saniye ~/ 10)}${(saniye % 10)}",
          style: TextStyle(color: FixedColor().color3, fontSize: 30),
        ),
        actions: [
          ElevatedButton(
            onPressed: () {
              _timer.cancel();
              ///////////////7-------------------------------------->>>>>>>>>>>>>>>< kişinin sınav bilgilerinin fb'ye yazıldığı yer
              addFbUserAnswers();

              gotoExamResult();

              choiceNotify.clearChoice();
            },
            child: const Text("Sınavı Bitir"),
          )
        ],
        centerTitle: true,
        backgroundColor: FixedColor().color1,
        shadowColor: FixedColor().color1,
        iconTheme: IconThemeData(
          color: FixedColor().color3,
        ),
      ),
      body: SingleChildScrollView(
        child: Stack(
          children: [
            StreamBuilder(
              stream: examRef,
              builder: (BuildContext context, AsyncSnapshot snapshot) {
                if (!snapshot.hasData) {
                  return const Center(
                    child: CircularProgressIndicator(),
                  );
                }
                List<DocumentSnapshot> examSnapList = snapshot.data.docs;
                return Center(
                  child: Column(
                    children: [
                      Container(
                        child: Center(
                            child: Text(
                          widget.whichTrialExam,
                          style: TextStyle(
                              fontSize:
                                  widget.whichTrialExam.length < 27 ? 30 : 20,
                              color: FixedColor().color1),
                        )),
                        color: FixedColor().color3,
                        height: MediaQuery.of(context).size.height * 0.082,
                        width: MediaQuery.of(context).size.width,
                      ),
                      Container(
                        margin: const EdgeInsets.symmetric(vertical: 8.0),
                        height: MediaQuery.of(context).size.height * 0.889,
                        child: ListView.builder(
                          ////////--------------------------------------------------------------->>>>>>>>>>>>>>>>>>>> soru sayısı 4 değil examSnapList.length olcak
                          itemCount: 40,
                          itemBuilder: (BuildContext context, int index) {
                            if (index == 0) {
                              return Column(
                                children: [
                                  Container(
                                    child: Center(
                                        child: Text(
                                      "Dhbt 1",
                                      style: TextStyle(
                                          fontSize:
                                              widget.whichTrialExam.length < 27
                                                  ? 30
                                                  : 20,
                                          color: FixedColor().color1),
                                    )),
                                    color: FixedColor().color3,
                                    height: MediaQuery.of(context).size.height *
                                        0.070,
                                    width: MediaQuery.of(context).size.width,
                                  ),
                                  soruBlogu(examSnapList, index),
                                ],
                              );
                            }
                            if (index == 20) {
                              return Column(
                                children: [
                                  Container(
                                    child: Center(
                                        child: Text(
                                      "Dhbt 2",
                                      style: TextStyle(
                                          fontSize:
                                              widget.whichTrialExam.length < 27
                                                  ? 30
                                                  : 20,
                                          color: FixedColor().color1),
                                    )),
                                    color: FixedColor().color3,
                                    height: MediaQuery.of(context).size.height *
                                        0.070,
                                    width: MediaQuery.of(context).size.width,
                                  ),
                                  soruBlogu(examSnapList, index),
                                ],
                              );
                            }

                            if (index == 39) {
                              return Column(
                                children: [
                                  soruBlogu(examSnapList, index),
                                  Divider(
                                    height: 8,
                                  ),
                                  Container(
                                    child: ElevatedButton(
                                      onPressed: () {
                                        _timer.cancel();
                                        addFbUserAnswers();
                                        gotoExamResult();
                                        choiceNotify.clearChoice();
                                      },
                                      child: const Text("Sınavı Bitir"),
                                    ),
                                  ),
                                  Container(
                                    height: 24,
                                  ),
                                  Container(
                                    height: 24,
                                  ),
                                  Container(
                                    height: 24,
                                  ),
                                ],
                              );
                            }
                            return soruBlogu(examSnapList, index);
                          },
                        ),
                      ),
                    ],
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }

  Column soruBlogu(List<DocumentSnapshot<Object?>> examSnapList, int index) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        ExamQuesContainer(
          quesnum: examSnapList[index]['soruNum'],
          quesinform: examSnapList[index]['soruOnBilgi'].toString(),
          ques: examSnapList[index]['soruIfade'].toString(),
          choicetext1: examSnapList[index]['a'].toString(),
          choicetext2: examSnapList[index]['b'].toString(),
          choicetext3: examSnapList[index]['c'].toString(),
          choicetext4: examSnapList[index]['d'].toString(),
          choicetext5: examSnapList[index]['e'].toString(),
          choicetext6: "Seçimi Boş Bırakmak İstiyorum",
        ),
      ],
    );
  }

  void gotoExamResult() {
    Navigator.pop(context);
    Navigator.push(context,
        MaterialPageRoute(builder: (BuildContext context) => ExamResult()));
  }

  void addFbUserAnswers() {
    List<Object> choiceList = choiceNotify.value.choiceMap.values.toList();
    List<String> choiceFinal = [
      " ",
      " ",
      " ",
      " ",
      " ",
      " ",
      " ",
      " ",
      " ",
      " ",
      " ",
      " ",
      " ",
      " ",
      " ",
      " ",
      " ",
      " ",
      " ",
      " ",
      " ",
      " ",
      " ",
      " ",
      " ",
      " ",
      " ",
      " ",
      " ",
      " ",
      " ",
      " ",
      " ",
      " ",
      " ",
      " ",
      " ",
      " ",
      " ",
      " "
    ];
    for (int i = 0; i < choiceList.length; i++) {
      if (choiceList[i] == 1) {
        choiceFinal[i] = "a";
      } else if (choiceList[i] == 2) {
        choiceFinal[i] = "b";
      } else if (choiceList[i] == 3) {
        choiceFinal[i] = "c";
      } else if (choiceList[i] == 4) {
        choiceFinal[i] = "d";
      } else if (choiceList[i] == 5) {
        choiceFinal[i] = "e";
      } else {
        choiceFinal[i] = " ";
      }
    }
    DocumentReference sinavDoc = _firestore
        .collection("kullanici")
        .doc(Hive.box("logindata").get("useruid").toString())
        .collection("cozulenSinavlar")
        .doc(widget.whichTrialExam);
    print(Hive.box("logindata").get("useruid").toString());
    sinavDoc.set({
      "cevaplar": [
        choiceFinal[0],
        choiceFinal[1],
        choiceFinal[2],
        choiceFinal[3],
        choiceFinal[4],
        choiceFinal[5],
        choiceFinal[6],
        choiceFinal[7],
        choiceFinal[8],
        choiceFinal[9],
        choiceFinal[10],
        choiceFinal[11],
        choiceFinal[12],
        choiceFinal[13],
        choiceFinal[14],
        choiceFinal[15],
        choiceFinal[16],
        choiceFinal[17],
        choiceFinal[18],
        choiceFinal[19],
        choiceFinal[20],
        choiceFinal[21],
        choiceFinal[22],
        choiceFinal[23],
        choiceFinal[24],
        choiceFinal[25],
        choiceFinal[26],
        choiceFinal[27],
        choiceFinal[28],
        choiceFinal[29],
        choiceFinal[30],
        choiceFinal[31],
        choiceFinal[32],
        choiceFinal[33],
        choiceFinal[34],
        choiceFinal[35],
        choiceFinal[36],
        choiceFinal[37],
        choiceFinal[38],
        choiceFinal[39]
      ]
    });
  }
}
