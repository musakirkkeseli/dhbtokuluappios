import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
// import 'package:page_transition/page_transition.dart';
// import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

import '../constants/color.dart';
// import '../model/duyuru_model.dart';
// import '../services/firebase_database.dart';
// import 'notification_page.dart';
// import 'package:webview_flutter/webview_flutter.dart';
// import 'dart:io';

class Sweepstake extends StatefulWidget {
  Sweepstake({Key? key}) : super(key: key);

  @override
  SsweepstakeState createState() => SsweepstakeState();
}

class SsweepstakeState extends State<Sweepstake> {
  late String isim = "", soyisim = "";
  @override
  void initState() {
    super.initState();
    // Enable hybrid composition.
    // if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();
    isim = Hive.box("logindata").get("isim") == null
        ? ""
        : Hive.box("logindata").get("isim").toString().toUpperCase();
    soyisim = Hive.box("logindata").get("isim") == null
        ? ""
        : Hive.box("logindata").get("soyisim").toString().toUpperCase();
  }

  @override
  Widget build(BuildContext context) {
    final Query _slideQuery =
        FirebaseFirestore.instance.collection('cekilis').orderBy('bitTar');
    Stream<QuerySnapshot> _slideStream = _slideQuery.snapshots();
    // final listenDocument =
    //     Provider.of<FirebaseDatabaseService>(context, listen: false);
    return Scaffold(
        appBar: AppBar(
          title: Column(
            children: [
              Text(
                "Merhaba",
                style: TextStyle(color: FixedColor().color2),
              ),
              Text(
                Hive.box("logindata").get("isim") == null
                    ? ""
                    : "$isim $soyisim",
                style: TextStyle(color: FixedColor().color3),
              )
            ],
          ),
          centerTitle: true,
          backgroundColor: FixedColor().color1,
          shadowColor: FixedColor().color1,
          iconTheme: IconThemeData(
            color: FixedColor().color3,
          ),
          // actions: [
          //   Padding(
          //       padding: const EdgeInsets.only(right: 5),
          //       child: StreamBuilder<List<dynamic>>(
          //           stream: listenDocument.duyuruStream(),
          //           builder: (BuildContext context,
          //               AsyncSnapshot<List<dynamic>> snapshot) {
          //             if (snapshot.hasError) {
          //               return const Icon(
          //                 Icons.notifications_none,
          //                 size: 40,
          //               );
          //             }

          //             if (snapshot.connectionState == ConnectionState.waiting) {
          //               return const Icon(
          //                 Icons.notifications_none,
          //                 size: 40,
          //               );
          //             }
          //             List<DuyuruModel>? docSnapList =
          //                 snapshot.data!.cast<DuyuruModel>();
          //             List? duyurular = Hive.box("logindata").get("duyurular");
          //             List duyurular1 = [];
          //             Function eq = const ListEquality().equals;
          //             bool equals = true;
          //             for (var i in docSnapList) {
          //               duyurular1.add(i.id);
          //             }
          //             if (eq(duyurular1, duyurular)) {
          //               equals = true;
          //               // } else if (duyurular!.length > duyurular1.length) {
          //               //   equals = true;
          //             } else {
          //               equals = false;
          //             }
          //             return IconButton(
          //               onPressed: () {
          //                 Navigator.push(
          //                     context,
          //                     PageTransition(
          //                         type: PageTransitionType.rightToLeft,
          //                         duration: const Duration(milliseconds: 500),
          //                         child: NotificationPage(
          //                           docSnapList: docSnapList,
          //                           duyurular1: duyurular1,
          //                         )));
          //               },
          //               icon: const Icon(
          //                 Icons.notifications_none,
          //                 size: 40,
          //               ),
          //               color: equals ? FixedColor().color2 : Colors.red,
          //             );
          //           }))
          // ],
        ),
        body: streamBuilderWidget(_slideStream));
  }

  StreamBuilder<QuerySnapshot<Object?>> streamBuilderWidget(
      Stream<QuerySnapshot<Object?>> _slideStream) {
    return StreamBuilder<QuerySnapshot>(
        stream: _slideStream,
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasError) {
            return const Text(
                'İnternet Bağlantınızı Kontrol Ederek Tekrar Deneyiniz!');
          }

          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(child: CircularProgressIndicator());
          }

          return listviewBuilderPadding(snapshot);
        });
  }

  listviewBuilderPadding(AsyncSnapshot<QuerySnapshot<Object?>> snapshot) {
    // print(snapshot.data!.docs.);
    Timestamp tarih =
        snapshot.data!.docs[snapshot.data!.docs.length - 1]['bitTar'];
    print(tarih.toDate());
    if (tarih.toDate().isBefore(DateTime.now())) {
      return const Padding(
        padding: EdgeInsets.symmetric(horizontal: 0.0),
        child: Center(child: Text("Gelecek Bir Tarihte Çekiliş Mevcut Değil")),
      );
    } else {
      return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 0.0),
        child: ListView.builder(
          scrollDirection: Axis.vertical,
          shrinkWrap: true,
          physics: const BouncingScrollPhysics(),
          itemCount: snapshot.data!.docs.length,
          itemBuilder: (BuildContext context, int index) {
            return _cardView(snapshot, index);
          },
        ),
      );
    }
    // return Padding(
    //   padding: const EdgeInsets.symmetric(horizontal: 0.0),
    //   child: snapshot.data!.docs.isEmpty
    //       ? const Center(
    //           child: Text("Gelecek Bir Tarihte Çekiliş Mevcut Değil"))
    //       : ListView.builder(
    //           scrollDirection: Axis.vertical,
    //           shrinkWrap: true,
    //           physics: const BouncingScrollPhysics(),
    //           itemCount: snapshot.data!.docs.length,
    //           itemBuilder: (BuildContext context, int index) {
    //             return _cardView(snapshot, index);
    //           },
    //         ),
    // );
  }

  Card _cardView(AsyncSnapshot<QuerySnapshot<Object?>> snapshot, index) {
    List<DocumentSnapshot> docSnapList = snapshot.data!.docs;

    if (!isBeforeToday(docSnapList[index]['bitTar'])) {
      return Card(
        child: ListTile(
          leading: Icon(
            Icons.sports_handball_outlined,
            color: FixedColor().color3,
          ),
          subtitle: cekilisAciklamaText(docSnapList, index),
          title: cekilisBaslikText(docSnapList, index),
          onTap: () {
            void _launchURL() async => await launch(docSnapList[index]['link']);
            _launchURL();
          },
        ),
      );
    } else {
      return const Card();
    }
  }

  // TextButton cekilisLinkTextButton(
  //     List<DocumentSnapshot<Object?>> docSnapList, index) {
  //   FixedColor myColor = FixedColor();
  //   return TextButton(
  //       child: Text(
  //         "Çekilişe Git",
  //         style: TextStyle(color: myColor.color3),
  //       ),
  //       onPressed: () {
  //         void _launchURL() async => await launch(docSnapList[index]['link']);
  //         _launchURL();
  //       });
  // }

  Text cekilisAciklamaText(List<DocumentSnapshot<Object?>> docSnapList, index) {
    return Text(
      docSnapList[index]['aciklama'],
    );
  }

  Text cekilisBaslikText(List<DocumentSnapshot<Object?>> docSnapList, index) {
    return Text(
      docSnapList[index]['baslik'],
      style: const TextStyle(fontWeight: FontWeight.bold),
    );
  }

  bool isBeforeToday(Timestamp timestamp) {
    return DateTime.now().toUtc().isAfter(
          DateTime.fromMillisecondsSinceEpoch(
            timestamp.millisecondsSinceEpoch,
            isUtc: false,
          ).toUtc(),
        );
  }
}
