import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
// import 'package:page_transition/page_transition.dart';
// import 'package:provider/provider.dart';

import '../constants/color.dart';
// import '../model/duyuru_model.dart';
import '../model/score_provider.dart';
// import '../services/firebase_database.dart';
import '../widgets/score_calculation_row.dart';
import '../widgets/score_calculation_score.dart';
import '../widgets/score_calculation_textform.dart';
// import 'notification_page.dart';

class ScoreCalculation extends StatefulWidget {
  const ScoreCalculation({Key? key}) : super(key: key);

  @override
  _ScoreCalculationState createState() => _ScoreCalculationState();
}

class _ScoreCalculationState extends State<ScoreCalculation> {
  final _formKey = GlobalKey<FormState>();
  double dhbt1Net = 0;
  double dhbt2Net = 0;
  double toplampuan = 0;
  double toplamnet = 0;
  @override
  Widget build(BuildContext context) {
    // final listenDocument =
    //     Provider.of<FirebaseDatabaseService>(context, listen: false);
    return Scaffold(
        appBar: AppBar(
          title: Column(
            children: [
              Text(
                "Merhaba",
                style: TextStyle(color: FixedColor().color2),
              ),
              Text(
                  Hive.box("logindata").get("isim") == null
                      ? ""
                      : "${Hive.box("logindata").get("isim").toString().toUpperCase()} ${Hive.box("logindata").get("soyisim").toString().toUpperCase()}",
                  style: TextStyle(color: FixedColor().color3))
            ],
          ),
          // actions: [
          //   Padding(
          //       padding: const EdgeInsets.only(right: 5),
          //       child: StreamBuilder<List<dynamic>>(
          //           stream: listenDocument.duyuruStream(),
          //           builder: (BuildContext context,
          //               AsyncSnapshot<List<dynamic>> snapshot) {
          //             if (snapshot.hasError) {
          //               return const Icon(
          //                 Icons.notifications_none,
          //                 size: 40,
          //               );
          //             }

          //             if (snapshot.connectionState == ConnectionState.waiting) {
          //               return const Icon(
          //                 Icons.notifications_none,
          //                 size: 40,
          //               );
          //             }
          //             List<DuyuruModel>? docSnapList =
          //                 snapshot.data!.cast<DuyuruModel>();
          //             List? duyurular = Hive.box("logindata").get("duyurular");
          //             List duyurular1 = [];
          //             Function eq = const ListEquality().equals;
          //             bool equals = true;
          //             for (var i in docSnapList) {
          //               duyurular1.add(i.id);
          //             }
          //             if (eq(duyurular1, duyurular)) {
          //               equals = true;
          //               // } else if (duyurular!.length > duyurular1.length) {
          //               //   equals = true;
          //             } else {
          //               equals = false;
          //             }
          //             return IconButton(
          //               onPressed: () {
          //                 Navigator.push(
          //                     context,
          //                     PageTransition(
          //                         type: PageTransitionType.rightToLeft,
          //                         duration: const Duration(milliseconds: 500),
          //                         child: NotificationPage(
          //                           docSnapList: docSnapList,
          //                           duyurular1: duyurular1,
          //                         )));
          //               },
          //               icon: const Icon(
          //                 Icons.notifications_none,
          //                 size: 40,
          //               ),
          //               color: equals ? FixedColor().color2 : Colors.red,
          //             );
          //           }))
          // ],
          centerTitle: true,
          backgroundColor: FixedColor().color1,
          shadowColor: FixedColor().color1,
          iconTheme: IconThemeData(
            color: FixedColor().color3,
          ),
        ),
        body: ValueListenableBuilder(
          valueListenable: scoreNotify,
          builder: (BuildContext context, ScoreTextModel value, Widget? child) {
            return SingleChildScrollView(
              child: Form(
                key: _formKey,
                child: Column(
                  children: [
                    Padding(
                      padding:
                          const EdgeInsets.fromLTRB(25.0, 40.0, 25.0, 25.0),
                      child: TextForm(text: "KPSS PUANI"),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(12.0),
                      child: TextFormRow(
                          text1: "DHBT 1 DOĞRU", text2: "DHBT 1 YANLIŞ"),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(12.0),
                      child: TextFormRow(
                          text1: "DHBT 2 DOĞRU", text2: "DHBT 2 YANLIŞ"),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: ElevatedButton(
                        onPressed: () {
                          if (_formKey.currentState!.validate()) {
                            _formKey.currentState!.save();
                            if ((value.scoreMap["DHBT 1 DOĞRU"] +
                                    value.scoreMap["DHBT 1 YANLIŞ"]) >
                                20) {
                              showAlertDialog(
                                  context,
                                  const Text(
                                      "DHBT 1 doğru ile yanlış toplamı en fazla 20 olabilir."));
                            } else if ((value.scoreMap["DHBT 2 DOĞRU"] +
                                    value.scoreMap["DHBT 2 YANLIŞ"]) >
                                20) {
                              showAlertDialog(
                                  context,
                                  const Text(
                                      "DHBT 2 doğru ile yanlış toplamı en fazla 20 olabilir."));
                            } else {
                              dhbt1Net = value.scoreMap["DHBT 1 DOĞRU"] -
                                  (value.scoreMap["DHBT 1 YANLIŞ"] * 0.25);
                              dhbt2Net = value.scoreMap["DHBT 2 DOĞRU"] -
                                  (value.scoreMap["DHBT 2 YANLIŞ"] * 0.25);
                              toplampuan =
                                  (value.scoreMap["KPSS PUANI"] * 0.3) +
                                      (0.7 *
                                          (32 +
                                              (dhbt1Net * 1.5) +
                                              (dhbt2Net * 1.9)));
                              toplamnet = dhbt1Net + dhbt2Net;
                            }
                          }
                        },
                        child: const Text("HESAPLA"),
                        style: ElevatedButton.styleFrom(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(50)),
                            fixedSize: Size(
                                MediaQuery.of(context).size.width * 0.8,
                                MediaQuery.of(context).size.height * 0.08),
                            shadowColor: FixedColor().color3),
                      ),
                    ),
                    Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 8.0),
                          child: Wrap(
                            // mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Score(
                                titletext: "DHBT 1 NETİ",
                                titlesize: 20,
                                scoretext: dhbt1Net,
                                scoresize: 30,
                              ),
                              Score(
                                titletext: "DHBT 2 NETİ",
                                titlesize: 20,
                                scoretext: dhbt2Net,
                                scoresize: 30,
                              ),
                              Score(
                                titletext: "TOPLAM NET",
                                titlesize: 20,
                                scoretext: toplamnet,
                                scoresize: 30,
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Score(
                              titletext: "KPSSDHBT PUANI",
                              titlesize: 20,
                              scoretext: toplampuan,
                              scoresize: 40),
                        )
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: RichText(
                        text: const TextSpan(
                          children: [
                            TextSpan(
                                text:
                                    "Daha önce dhbt sınavına girip bir puan sonucu elde ettiyseniz puan hesaplama sistemimize katkıda bulunabilirsiniz. Lütfen sınav sonuç belgenizi",
                                style: TextStyle(color: Colors.black)),
                            WidgetSpan(
                                child: SelectableText(" bilgi@dhbtokulu.com",
                                    style: TextStyle(color: Colors.blue))),
                            TextSpan(
                                text: "adresine gönderin.",
                                style: TextStyle(color: Colors.black)),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            );
          },
        ));
  }

  showAlertDialog(BuildContext context, title) {
    return showDialog(
        builder: (BuildContext context) {
          return AlertDialog(
            title: title,
            actions: [
              TextButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: const Text("Tamam"))
            ],
          );
        },
        context: context);
  }
}
