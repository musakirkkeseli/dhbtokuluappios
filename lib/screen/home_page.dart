import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import '../constants/color.dart';
import '../model/home_page_button.dart';
import '../model/slide_item_count.dart';
import '../model/timer_provider.dart';
import '../widgets/home_page_button_item.dart';
import '../widgets/slide_dots.dart';
import '../widgets/slide_item.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _currentPage = 0;
  late int slideItemCounter = 2;
  final PageController _pageController = PageController(initialPage: 0);

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _pageController.dispose();
  }

  _onPageChanged(int index) {
    setState(() {
      _currentPage = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder(
        valueListenable: slideItemNotify,
        builder: (BuildContext context, StaticSlideItemCount modelim, child) {
          return Scaffold(
              body: Column(
            children: <Widget>[
              timerExpanded(context),
              slideIcerigiExpanded(context),
              menuButonlariExpanded(),
            ],
          ));
        });
  }

  timerExpanded(BuildContext context) {
    return FutureBuilder<DocumentSnapshot>(
        future: FirebaseFirestore.instance
            .collection('zaman')
            .doc("sinavzamani")
            .get(),
        builder:
            (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
          if (snapshot.hasError) {
            return const Text("Bir şeyler yanlış gitti.");
          }

          if (snapshot.hasData && !snapshot.data!.exists) {
            return const Text("Belge mevcut değil");
          }

          if (snapshot.connectionState == ConnectionState.done) {
            Map<String, dynamic> data =
                snapshot.data!.data() as Map<String, dynamic>;

            if (data['tarih'] == null || data["aktif"] == false) {
              return const SizedBox();
            } else {
              Timestamp tarih = data['tarih'];
              late Timer _timer;
              const onesec = Duration(seconds: 1);
              _timer = Timer.periodic(onesec, (timer) {
                timeNotify.onfinishTime(tarih);
              });
              return ValueListenableBuilder(
                  valueListenable: timeNotify,
                  builder:
                      (BuildContext context, FinishTime value, Widget? child) {
                    return Container(
                      color: FixedColor().color3,
                      child: Column(
                        children: [
                          Padding(
                            padding:
                                const EdgeInsets.fromLTRB(8.0, 13.0, 8.0, 3.0),
                            child: Text(
                              data['baslik'],
                              style: TextStyle(
                                  fontSize: 25,
                                  fontWeight: FontWeight.bold,
                                  color: FixedColor().color1),
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Padding(
                                padding: const EdgeInsets.fromLTRB(
                                    8.0, 3.0, 8.0, 13.0),
                                child: Column(
                                  children: [
                                    Text(
                                      "${value.gun}",
                                      style: TextStyle(
                                          fontSize: 30,
                                          color: FixedColor().color1),
                                    ),
                                    Text(
                                      "GÜN",
                                      style: TextStyle(
                                          fontSize: 10,
                                          color: FixedColor().color1),
                                    ),
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.fromLTRB(
                                    8.0, 3.0, 8.0, 13.0),
                                child: Column(
                                  children: [
                                    Text(
                                      value.saat.toString().padLeft(2, "0"),
                                      style: TextStyle(
                                          fontSize: 30,
                                          color: FixedColor().color1),
                                    ),
                                    Text(
                                      "SAAT",
                                      style: TextStyle(
                                          fontSize: 10,
                                          color: FixedColor().color1),
                                    ),
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.fromLTRB(
                                    8.0, 3.0, 8.0, 13.0),
                                child: Column(
                                  children: [
                                    Text(
                                      value.dakika.toString().padLeft(2, "0"),
                                      style: TextStyle(
                                          fontSize: 30,
                                          color: FixedColor().color1),
                                    ),
                                    Text(
                                      "DAKİKA",
                                      style: TextStyle(
                                          fontSize: 10,
                                          color: FixedColor().color1),
                                    ),
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.fromLTRB(
                                    8.0, 3.0, 8.0, 13.0),
                                child: Column(
                                  children: [
                                    Text(
                                      value.saniye.toString().padLeft(2, "0"),
                                      style: TextStyle(
                                          fontSize: 30,
                                          color: FixedColor().color1),
                                    ),
                                    Text(
                                      "SANİYE",
                                      style: TextStyle(
                                          fontSize: 10,
                                          color: FixedColor().color1),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    );
                  });
            }
          }

          return const Center(
            child: CircularProgressIndicator(),
          );
        });
  }

  Expanded slideIcerigiExpanded(BuildContext context) {
    return Expanded(
      flex: 3,
      child: SizedBox(
        // width: MediaQuery.of(context).size.width,
        // height: MediaQuery.of(context).size.height * 0.25,
        child: StreamBuilder(
            stream: FirebaseFirestore.instance.collection('slide').snapshots(),
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              if (!snapshot.hasData) {
                return const Center(child: CircularProgressIndicator());
              } else {
                slideItemNotify.value.itemCount = snapshot.data.docs.length;
                // return Column(children: [
                //     PageView.builder(
                //   itemBuilder: (ctx, i) => SlideItem(i),
                //   // itemCount: snapshot.data!.toList().length,
                //   itemCount: snapshot.data.docs.length,
                //   scrollDirection: Axis.horizontal,
                //   controller: _pageController,
                //   onPageChanged: _onPageChanged,
                // ),
                // slideNoktalariExpanded(),
                // ],);
                //
                return Column(
                  children: [
                    Expanded(
                      child: SizedBox(
                        // height: 200.0,
                        child: PageView.builder(
                          itemBuilder: (ctx, i) => SlideItem(i),
                          // itemCount: snapshot.data!.toList().length,
                          itemCount: snapshot.data.docs.length,
                          scrollDirection: Axis.horizontal,
                          controller: _pageController,
                          onPageChanged: _onPageChanged,
                        ),
                      ),
                    ),
                    slideNoktalariExpanded(),
                  ],
                );
              }
            }),
      ),
    );
  }

  Row slideNoktalariExpanded() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        for (int i = 0; i < slideItemNotify.value.itemCount; i++)
          if (i == _currentPage) SlideDots(true) else SlideDots(false)
      ],
    );
  }

  Expanded menuButonlariExpanded() {
    return Expanded(
      flex: 8,
      child: GridView.builder(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            childAspectRatio: 2 / 1,
            crossAxisSpacing: MediaQuery.of(context).size.width * 0.01,
            mainAxisSpacing: MediaQuery.of(context).size.height * 0.001),
        itemCount: homebuttonList.length,
        itemBuilder: (
          context,
          index,
        ) {
          return Padding(
            padding: const EdgeInsets.all(8.0),
            child: HomeButtonItem(index),
          );
        },
      ),
    );
  }
}
