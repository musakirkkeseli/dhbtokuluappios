// exam_answer_key.dart

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dhbtokulu/constants/color.dart';
import 'package:dhbtokulu/model/answer_key_filter_provider.dart';
import 'package:dhbtokulu/model/trial_exam_provider.dart';
import 'package:dhbtokulu/widgets/answer_key_container.dart';
import 'package:dhbtokulu/widgets/answer_key_filter_button.dart';
import 'package:flutter/material.dart';

class ExamAnswerKey extends StatefulWidget {
  late List<int> trueQueList;
  late List<int> falseQueList;
  late List<int> blankQueList;
  late int dogruCevapSayisi;
  late int yanlisCevapSayisi;
  late int bosCevapSayisi;
  ExamAnswerKey(
      {Key? key,
      required this.dogruCevapSayisi,
      required this.yanlisCevapSayisi,
      required this.trueQueList,
      required this.falseQueList,
      required this.blankQueList,
      required this.bosCevapSayisi})
      : super(key: key);

  @override
  _ExamAnswerKeyState createState() => _ExamAnswerKeyState();
}

class _ExamAnswerKeyState extends State<ExamAnswerKey> {
  int rowNum1 = 1;
  int rowNum2 = 2;
  int rowNum3 = 3;
  @override
  Widget build(BuildContext context) {
    final _firestore = FirebaseFirestore.instance;

    Stream<QuerySnapshot> examRef = _firestore
        .collection("deneme")
        .doc(choiceNotify.value.examName)
        .collection("sorular")
        .orderBy('soruNum', descending: false)
        .snapshots();
    return ValueListenableBuilder(
        valueListenable: answerChoiceNotify,
        builder: (BuildContext context, AnswerKeyFilterChoice model, child) {
          return Scaffold(
            appBar: AppBar(
              title: Text(
                "Cevap Anahtarı",
                style: TextStyle(color: FixedColor().color3, fontSize: 30),
              ),
              centerTitle: true,
              backgroundColor: FixedColor().color1,
              shadowColor: FixedColor().color1,
              iconTheme: IconThemeData(
                color: FixedColor().color3,
              ),
            ),
            body: SingleChildScrollView(
              child: Column(
                children: [
                  Row(
                    children: [
                      Expanded(
                          child: FilterButton(
                        icon: Icon(
                          Icons.check_circle,
                          color: FixedColor().color3,
                        ),
                        rownum: rowNum1,
                        filterofnum: widget.dogruCevapSayisi,
                      )),
                      Expanded(
                          child: FilterButton(
                        icon: Icon(
                          Icons.cancel,
                          color: FixedColor().color2,
                        ),
                        rownum: rowNum2,
                        filterofnum: widget.yanlisCevapSayisi,
                      )),
                      Expanded(
                          child: FilterButton(
                        icon: Icon(
                          Icons.error,
                          color: FixedColor().color4,
                        ),
                        rownum: rowNum3,
                        filterofnum: widget.bosCevapSayisi,
                      )),
                    ],
                  ),
                  Container(
                    child: Center(
                        child: Text(
                      "${choiceNotify.value.examName.toUpperCase()}",
                      style: TextStyle(
                          fontSize:
                              choiceNotify.value.examName.length < 20 ? 30 : 15,
                          color: FixedColor().color1),
                    )),
                    color: FixedColor().color3,
                    height: MediaQuery.of(context).size.height * 0.08,
                    width: MediaQuery.of(context).size.width,
                  ),

                  StreamBuilder<Object>(
                      stream: examRef,
                      builder: (BuildContext context, AsyncSnapshot snapshot) {
                        if (!snapshot.hasData) {
                          return const Center(
                            child: CircularProgressIndicator(),
                          );
                        }
                        List<DocumentSnapshot> examSnapList =
                            snapshot.data.docs;
                        print("yeni sayfada dogru cevap:${widget.trueQueList}");
                        print("yeni sayfada boş cevap:${widget.blankQueList}");
                        print(
                            "yeni sayfada yanlış cevap:${widget.falseQueList}");

                        return Container(
                          margin: const EdgeInsets.symmetric(vertical: 8.0),
                          height: MediaQuery.of(context).size.height * 0.750,
                          child: ListView.builder(
                            ////////--------------------------------------------------------------->>>>>>>>>>>>>>>>>>>> soru sayısı 4 değil examSnapList.length olcak
                            itemCount: examSnapList.length,
                            itemBuilder: (BuildContext context, int index) {
                              // doğru tap butonu olduğu
                              if (answerChoiceNotify.value.selectedIndex == 1) {
                                //doğru cevap olup olmadığı
                                if (widget.trueQueList.isNotEmpty) {
                                  // indisi no ile doğru listesindeki indis tutuyorsa
                                  if (index == widget.trueQueList[index]) {
                                    return Column(
                                      mainAxisSize: MainAxisSize.min,
                                      children: [
                                        AnswerKeyContainer(
                                            quesnum: 1 + index,
                                            quesinform: examSnapList[index]
                                                    ['soruOnBilgi']
                                                .toString(),
                                            ques: examSnapList[index]
                                                    ['soruIfade']
                                                .toString(),
                                            choicetext1: examSnapList[index]
                                                    ['a']
                                                .toString(),
                                            choicetext2: examSnapList[index]
                                                    ['b']
                                                .toString(),
                                            choicetext3: examSnapList[index]
                                                    ['c']
                                                .toString(),
                                            choicetext4: examSnapList[index]
                                                    ['d']
                                                .toString(),
                                            choicetext5: examSnapList[index]
                                                    ['e']
                                                .toString()),
                                      ],
                                    );
                                  }
                                }
                                return const Text("");
                                // yanlış cevapları getiren
                              } else if (answerChoiceNotify
                                      .value.selectedIndex ==
                                  2) {
                                if (widget.falseQueList.isNotEmpty) {
                                  if (index == widget.falseQueList[index]) {
                                    return Column(
                                      mainAxisSize: MainAxisSize.min,
                                      children: [
                                        AnswerKeyContainer(
                                            quesnum: 1 + index,
                                            quesinform: examSnapList[index]
                                                    ['soruOnBilgi']
                                                .toString(),
                                            ques: examSnapList[index]
                                                    ['soruIfade']
                                                .toString(),
                                            choicetext1: examSnapList[index]
                                                    ['a']
                                                .toString(),
                                            choicetext2: examSnapList[index]
                                                    ['b']
                                                .toString(),
                                            choicetext3: examSnapList[index]
                                                    ['c']
                                                .toString(),
                                            choicetext4: examSnapList[index]
                                                    ['d']
                                                .toString(),
                                            choicetext5: examSnapList[index]
                                                    ['e']
                                                .toString()),
                                      ],
                                    );
                                  }
                                }
                                return const Text("");
                                // boş cevapları getiren
                              } else if (answerChoiceNotify
                                      .value.selectedIndex ==
                                  3) {
                                if (widget.blankQueList.isNotEmpty) {
                                  if (index == widget.blankQueList[index]) {
                                    return Column(
                                      mainAxisSize: MainAxisSize.min,
                                      children: [
                                        AnswerKeyContainer(
                                            quesnum: 1 + index,
                                            quesinform: examSnapList[index]
                                                    ['soruOnBilgi']
                                                .toString(),
                                            ques: examSnapList[index]
                                                    ['soruIfade']
                                                .toString(),
                                            choicetext1: examSnapList[index]
                                                    ['a']
                                                .toString(),
                                            choicetext2: examSnapList[index]
                                                    ['b']
                                                .toString(),
                                            choicetext3: examSnapList[index]
                                                    ['c']
                                                .toString(),
                                            choicetext4: examSnapList[index]
                                                    ['d']
                                                .toString(),
                                            choicetext5: examSnapList[index]
                                                    ['e']
                                                .toString()),
                                      ],
                                    );
                                  }
                                }
                                return const Text("");
                                // return Text("Boş Cevaplar Gösterilecek >${index}<, boş cevap listesi>${widget.blankQueList}<");
                              } else {
                                return Column(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    AnswerKeyContainer(
                                        quesnum: 1 + index,
                                        quesinform: examSnapList[index]
                                                ['soruOnBilgi']
                                            .toString(),
                                        ques: examSnapList[index]['soruIfade']
                                            .toString(),
                                        choicetext1:
                                            examSnapList[index]['a'].toString(),
                                        choicetext2:
                                            examSnapList[index]['b'].toString(),
                                        choicetext3:
                                            examSnapList[index]['c'].toString(),
                                        choicetext4:
                                            examSnapList[index]['d'].toString(),
                                        choicetext5: examSnapList[index]['e']
                                            .toString()),
                                  ],
                                );
                              }
                            },
                          ),
                        );
                      }),

                  ///////////////////// sınav formatını unutma bu şekilde olcak üstte dhbt1 altta dhbt2 sınavları
                ],
              ),
            ),
          );
        });
  }
}
