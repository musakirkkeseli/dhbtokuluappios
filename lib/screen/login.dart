import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dhbtokulu/screen/home_page.dart';
import 'package:dhbtokulu/screen/trial_exam.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:page_transition/page_transition.dart';

import '../constants/color.dart';
import '../model/auth_model.dart';
import '../widgets/custom_dialog_box.dart';
import '../widgets/navbar.dart';
import 'register.dart';
import 'reset_page.dart';

class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> with SingleTickerProviderStateMixin {
  bool isChecked = false;
  final _formKey = GlobalKey<FormState>();
  final _emailRegExp = RegExp(
      r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+");

  final formmodel = AuthFormModel();

  TabController? _controller;

  @override
  void initState() {
    // TODO: implement initStatef
    super.initState();
    _controller = TabController(length: 2, vsync: this, initialIndex: 0);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Giriş",
          style: TextStyle(color: FixedColor().color3),
        ),
        centerTitle: true,
        backgroundColor: FixedColor().color1,
        iconTheme: IconThemeData(
          color: FixedColor().color3,
        ),
        shadowColor: FixedColor().color1,
      ),
      body: SingleChildScrollView(
        child: Form(key: _formKey, child: emailLoginColumn(context)),
      ),
    );
  }

  Column emailLoginColumn(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        hosgeldinizYazisi(),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              TextFormField(
                maxLength: 30,
                validator: (value) {
                  if (value!.isEmpty) {
                    return "Lütfen bir e-posta adresi giriniz.";
                  } else if (!_emailRegExp.hasMatch(value)) {
                    return 'Geçersiz e-posta adresi!';
                  }
                  return null;
                },
                onSaved: (value) {
                  formmodel.emailAddress = value!;
                },
                onChanged: (value) {
                  setState(() {
                    formmodel.emailAddress = value;
                  });
                },
                keyboardType: TextInputType.emailAddress,
                decoration: InputDecoration(
                    fillColor: Colors.transparent,
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                          color: FixedColor().color3,
                          style: BorderStyle.solid,
                          width: 2),
                    ),
                    labelText: "E-Posta",
                    labelStyle: TextStyle(color: FixedColor().color2)),
              ),
              TextFormField(
                maxLength: 30,
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Lütfen şifrenizi giriniz.';
                  } else if (value.toString().length < 6) {
                    return 'Şifre 6 karakterden az olamaz.';
                  }
                  return null;
                },
                onSaved: (value) {
                  formmodel.password = value!;
                },
                onChanged: (value) {
                  setState(() {
                    formmodel.password = value;
                  });
                },
                obscureText: true,
                decoration: InputDecoration(
                    fillColor: Colors.transparent,
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                          color: FixedColor().color3,
                          style: BorderStyle.solid,
                          width: 2),
                    ),
                    labelText: "Sifre",
                    labelStyle: TextStyle(color: FixedColor().color2)),
              ),
            ],
          ),
        ),
        girisYapButonu(context),
        sifremiUnuttumRow(context),
        hesabinYokmuKaydolRow(context)
      ],
    );
  }

  Padding hosgeldinizYazisi() {
    return const Padding(
      padding: EdgeInsets.all(20.0),
      child: Text(
        "Hoşgeldiniz!",
        style: TextStyle(fontSize: 30),
      ),
    );
  }

  Padding girisYapButonu(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 15),
      child: Center(
        child: ElevatedButton(
          onPressed: () {
            if (_formKey.currentState!.validate()) {
              _formKey.currentState!.save();
              _firebaseSignInFunc(context);
            }
          },
          child: const Text("Giriş Yap", style: TextStyle(fontSize: 17)),
          style: ElevatedButton.styleFrom(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(50)),
            primary: FixedColor().color3,
            fixedSize: Size(MediaQuery.of(context).size.width * 0.9,
                MediaQuery.of(context).size.height * 0.055),
          ),
        ),
      ),
    );
  }

  _firebaseSignInFunc(BuildContext context) async {
    print(formmodel.emailAddress);
    print(formmodel.password);
    try {
      await FirebaseAuth.instance
          .signInWithEmailAndPassword(
              email: formmodel.emailAddress, password: formmodel.password)
          .then((user) {
        getUserData(user.user!.uid);
      });
    } on FirebaseAuthException catch (e) {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return CustomDialogBox(
              title: "Giriş Hatası",
              descriptions:
                  "Dhbt Okulu'na Giriş Sırasında Eposta/Parola Hatası Meydana Geldi",
              text: "Tekrar Dene",
            );
          });
      if (e.code == 'user-not-found') {
        print('No user found for that email.');
      } else if (e.code == 'wrong-password') {
        print('Wrong password provided for that user.');
      }
    }
  }

  void getUserData(useruid) {
    FirebaseFirestore.instance
        .collection('kullanici')
        .doc(useruid)
        .get()
        .then((DocumentSnapshot snapshot) {
      Map doc = snapshot.data() as Map<String, dynamic>;
      Hive.box("logindata").putAll({
        "useruid": useruid,
        "isim": doc['isim'],
        "soyisim": doc['soyisim']
      }).then((value) => Navigator.of(context).pushNamedAndRemoveUntil(
              "/trialexam", ModalRoute.withName("/navbar"))
          // Navigator.pushAndRemoveUntil(
          //         context,
          //         MaterialPageRoute(
          //             builder: (BuildContext context) => const TrialExam()),
          //         ModalRoute.withName("/navbar"))
          // Navigator.pushAndRemoveUntil(
          //     context,
          //     MaterialPageRoute(builder: (context) => const Navbar()),
          //     (Route<dynamic> route) => false)
          );
    });
  }

  Row sifremiUnuttumRow(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        TextButton(
          child: Text(
            "Şifremi Unuttum ",
            style: TextStyle(color: FixedColor().color3),
          ),
          onPressed: () => Navigator.push(
              context,
              PageTransition(
                  type: PageTransitionType.bottomToTop,
                  duration: const Duration(seconds: 1),
                  child: ResetPage(
                    appbarText: "Şifremi Unuttum",
                  ))),
        )
      ],
    );
  }

  Row hesabinYokmuKaydolRow(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          "Hesabın yok mu?",
          style: TextStyle(color: FixedColor().color2),
        ),
        TextButton(
          onPressed: () {
            Navigator.push(
                context,
                PageTransition(
                    type: PageTransitionType.bottomToTop,
                    duration: const Duration(seconds: 1),
                    child: const Register()));
          },
          child: Text(
            "Kayıt Ol",
            style: TextStyle(color: FixedColor().color3),
          ),
        )
      ],
    );
  }
}
