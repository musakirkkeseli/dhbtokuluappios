import 'package:dhbtokulu/screen/home_page.dart';
import 'package:dhbtokulu/screen/trial_exam.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:onesignal_flutter/onesignal_flutter.dart';
import 'package:provider/provider.dart';

import 'screen/login_page.dart';
import 'services/firebase_database.dart';
import 'widgets/navbar.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  await Hive.initFlutter();
  await Hive.openBox('logindata');
  // await Hive.box("logindata").clear();

  //Remove this method to stop OneSignal Debugging
  OneSignal.shared.setLogLevel(OSLogLevel.verbose, OSLogLevel.none);

  OneSignal.shared.setAppId("7db393b5-72b9-4c68-831c-c3ae2376888e");

  // The promptForPushNotificationsWithUserResponse function will show the iOS push notification prompt. We recommend removing the following code and instead using an In-App Message to prompt for notification permission
  OneSignal.shared.promptUserForPushNotificationPermission().then((accepted) {
    print("Accepted permission: $accepted");
  });

  runApp(MultiProvider(
    providers: [
      Provider<FirebaseDatabaseService>(
        create: (_) => FirebaseDatabaseService(),
      ),
    ],
    child: MaterialApp(
      debugShowCheckedModeBanner: false,
      routes: {
        "/navbar": (context) => Navbar(),
        "/trialexam": (context) => TrialExam()
      },
      initialRoute: "/navbar",
    ),
  ));
}

class UserActivityQuery extends StatefulWidget {
  const UserActivityQuery({Key? key}) : super(key: key);

  @override
  _UserActivityQueryState createState() => _UserActivityQueryState();
}

class _UserActivityQueryState extends State<UserActivityQuery> {
  @override
  Widget build(BuildContext context) {
    return const Navbar();
  }
}
