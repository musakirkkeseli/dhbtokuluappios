import '../model/duyuru_model.dart';
import '../model/hapbilgi_model.dart';
import 'firebase_services.dart';

class FirebaseDatabaseService {
  final _firestoreService = FirestoreService.instance;
  Stream<List<HapBilgiModel>> roomsStream() =>
      _firestoreService.collectionStream(
        path1: "hapBilgiler",
        orderBy: "createdAt",
        descending: false,
        builder: (data, documentId) => HapBilgiModel.fromMap(data, documentId),
      );

  Stream<List<DuyuruModel>> duyuruStream() =>
      _firestoreService.collectionStream(
        path1: "duyurular",
        orderBy: "tarih",
        descending: true,
        builder: (data, documentId) => DuyuruModel.fromMap(data, documentId),
      );
}
