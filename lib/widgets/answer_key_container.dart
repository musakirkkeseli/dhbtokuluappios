// deneme bitti, doğru yanlışlarına bakacak burada doğru yanlışları boyuyuoruz

import 'package:dhbtokulu/constants/color.dart';
import 'package:dhbtokulu/model/auth_user_values_provider.dart';
import 'package:dhbtokulu/widgets/answer_key_answer_button.dart';
import 'package:flutter/material.dart';

class AnswerKeyContainer extends StatefulWidget {
  late int quesnum;
  late String quesinform;
  late String ques;
  late String choicetext1;
  late String choicetext2;
  late String choicetext3;
  late String choicetext4;
  late String choicetext5;
  AnswerKeyContainer({
    Key? key,
    required this.quesnum,
    this.quesinform = "",
    required this.ques,
    required this.choicetext1,
    required this.choicetext2,
    required this.choicetext3,
    required this.choicetext4,
    required this.choicetext5,
  }) : super(key: key);

  @override
  _AnswerKeyContainerState createState() => _AnswerKeyContainerState();
}

class _AnswerKeyContainerState extends State<AnswerKeyContainer> {
  //0 white, 1 greeen, 2 red, 3 grey
  late int aTrue = 0, bTrue = 0, cTrue = 0, dTrue = 0, eTrue = 0;
  @override
  Widget build(BuildContext context) {
    late Color choiceColor = Colors.white;

    late String whichKey = 'a';
    bool onbilgi = true;

    //print("------------------------------------------------------------------------>>>Yukarı quesnum:${widget.quesnum} ");

    if (authNotify.value.kisiCevap[widget.quesnum - 1] ==
        authNotify.value.cevapAnahtari[widget.quesnum - 1]) {
      whichKey = authNotify.value.kisiCevap[widget.quesnum - 1];
      isTrue(whichKey);
    } else if (authNotify.value.kisiCevap[widget.quesnum - 1] !=
        authNotify.value.cevapAnahtari[widget.quesnum - 1]) {
      if (authNotify.value.kisiCevap[widget.quesnum - 1] == " ") {
        whichKey = authNotify.value.cevapAnahtari[widget.quesnum - 1];
        isGrey(whichKey);
      } else {
        whichKey = authNotify.value.kisiCevap[widget.quesnum - 1];
        isFalse(whichKey);
      }
    }

    if (authNotify.value.kisiCevap[widget.quesnum - 1] == " ") {
    } else {
      if (authNotify.value.cevapAnahtari[widget.quesnum - 1] == "a") {
        aTrue = 1;
      } else if (authNotify.value.cevapAnahtari[widget.quesnum - 1] == "b") {
        bTrue = 1;
      } else if (authNotify.value.cevapAnahtari[widget.quesnum - 1] == "c") {
        cTrue = 1;
      } else if (authNotify.value.cevapAnahtari[widget.quesnum - 1] == "d") {
        dTrue = 1;
      } else if (authNotify.value.cevapAnahtari[widget.quesnum - 1] == "e") {
        eTrue = 1;
      }
    }

    //   if(authNotify.value.kisiCevap[widget.quesnum-1] == " "){
    //    aTrue = 3;
    // }else if(authNotify.value.kisiCevap[widget.quesnum-1] == " "){
    //   bTrue = 3;
    // }else if(authNotify.value.kisiCevap[widget.quesnum-1] == " "){
    //   cTrue = 3;
    // }else if(authNotify.value.kisiCevap[widget.quesnum-1] == " "){
    //   dTrue = 3;
    // }else if(authNotify.value.kisiCevap[widget.quesnum-1] == " "){
    //   eTrue = 3;
    // }

    EdgeInsets quesinfoPadding() {
      if (widget.quesinform == "") {
        return const EdgeInsets.all(0.0);
      } else if (widget.quesinform == " ") {
        return const EdgeInsets.all(0.0);
      } else {
        return const EdgeInsets.all(20.0);
      }
    }

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
            padding: const EdgeInsetsDirectional.only(
                top: 15, start: 20, bottom: 15),
            child: Text(
              "Soru ${widget.quesnum}",
              style: TextStyle(color: FixedColor().color3, fontSize: 25),
            )),
        Divider(
          color: FixedColor().color3,
          height: 0,
          indent: 20,
          endIndent: 20,
          thickness: 2,
        ),
        Padding(
          padding: quesinfoPadding(),
          child: Text(widget.quesinform),
        ),
        Padding(
          padding: const EdgeInsets.all(20.0),
          child: Text(
            widget.ques,
            style: const TextStyle(fontWeight: FontWeight.bold),
          ),
        ),
        AnswerButton(
          quesnum: widget.quesnum,
          choicenum: 1,
          choicetext: widget.choicetext1,
          trueValue: aTrue,
        ),
        AnswerButton(
          quesnum: widget.quesnum,
          choicenum: 2,
          choicetext: widget.choicetext2,
          trueValue: bTrue,
        ),
        AnswerButton(
          quesnum: widget.quesnum,
          choicenum: 3,
          choicetext: widget.choicetext3,
          trueValue: cTrue,
        ),
        AnswerButton(
          quesnum: widget.quesnum,
          choicenum: 4,
          choicetext: widget.choicetext4,
          trueValue: dTrue,
        ),
        AnswerButton(
          quesnum: widget.quesnum,
          choicenum: 5,
          choicetext: widget.choicetext5,
          trueValue: eTrue,
        ),
      ],
    );
  }

  void isFalse(String whichKey) {
    if (whichKey == "a") {
      aTrue = 2;
    } else if (whichKey == "b") {
      bTrue = 2;
    } else if (whichKey == "c") {
      cTrue = 2;
    } else if (whichKey == "d") {
      dTrue = 2;
    } else if (whichKey == "e") {
      eTrue = 2;
    }
  }

  void isBlank(String whichKey) {
    if (whichKey == "a") {
      aTrue = 0;
    } else if (whichKey == "b") {
      bTrue = 0;
    } else if (whichKey == "c") {
      cTrue = 0;
    } else if (whichKey == "d") {
      dTrue = 0;
    } else if (whichKey == "e") {
      eTrue = 0;
    }
  }

  void isTrue(String whichKey) {
    if (whichKey == "a") {
      aTrue = 1;
    } else if (whichKey == "b") {
      bTrue = 1;
    } else if (whichKey == "c") {
      cTrue = 1;
    } else if (whichKey == "d") {
      dTrue = 1;
    } else if (whichKey == "e") {
      eTrue = 1;
    }
  }

  void isGrey(String whichKey) {
    if (whichKey == "a") {
      aTrue = 3;
    } else if (whichKey == "b") {
      bTrue = 3;
    } else if (whichKey == "c") {
      cTrue = 3;
    } else if (whichKey == "d") {
      dTrue = 3;
    } else if (whichKey == "e") {
      eTrue = 3;
    }
  }
}
