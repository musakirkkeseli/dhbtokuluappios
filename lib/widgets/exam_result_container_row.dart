import 'package:flutter/material.dart';

class ExamResultContainerRow extends StatelessWidget {
  late Icon icon;
  late String text;
  late int sayi;
  ExamResultContainerRow(
      {Key? key, required this.icon, required this.text, required this.sayi})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: icon,
        ),
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: Text(text),
        ),
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: Text(
            sayi.toString(),
            style: const TextStyle(fontSize: 30),
          ),
        )
      ],
    );
  }
}
