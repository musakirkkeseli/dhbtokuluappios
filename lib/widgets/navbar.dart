import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
// import 'package:page_transition/page_transition.dart';
// import 'package:provider/provider.dart';

import '../constants/color.dart';
// import '../model/duyuru_model.dart';
import '../model/launch_time_provider.dart';
import '../model/navbar_provider.dart';
// import '../screen/notification_page.dart';
// import '../services/firebase_database.dart';
import 'getTimes.dart';

class Navbar extends StatefulWidget {
  const Navbar({Key? key}) : super(key: key);

  @override
  _NavbarState createState() => _NavbarState();
}

class _NavbarState extends State<Navbar> with WidgetsBindingObserver {
  DateTime? _startTime;
  @override
  void initState() {
    WidgetsBinding.instance!.addObserver(this);
    _startTime = DateTime.now();
    launchNotify.value.time = getTimes().getTheTimes();
    super.initState();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    switch (state) {
      case AppLifecycleState.paused:
        int? pastTime = Hive.box("logindata").get("apptime");
        print("pastTime -------------------------> ${pastTime}");
        int _totalTime = pastTime == null
            ? DateTime.now().difference(_startTime!).inMinutes
            : pastTime + DateTime.now().difference(_startTime!).inMinutes;
        print("totaltime ----------------------------------> ${_totalTime}");
        Hive.box("logindata").put("apptime", _totalTime);
        return;
      case AppLifecycleState.resumed:
        _startTime = DateTime.now();
        launchNotify.value.time = getTimes().getTheTimes();
        return;
      default:
        return;
    }
  }

  @override
  void dispose() {
    WidgetsBinding.instance!.removeObserver(this);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder(
      valueListenable: selectedNotify,
      builder: (BuildContext context, ItemTapped model, child) {
        // final listenDocument =
        //     Provider.of<FirebaseDatabaseService>(context, listen: false);
        return Scaffold(
          appBar: AppBar(
            title: Column(
              children: [
                Text(
                  "Merhaba",
                  style: TextStyle(color: FixedColor().color2),
                ),
                ValueListenableBuilder(
                    valueListenable: Hive.box("logindata").listenable(),
                    builder: (context, Box value, child) {
                      return Text(
                        value.get("isim") == null
                            ? ""
                            : "${Hive.box("logindata").get("isim").toString().toUpperCase()} ${Hive.box("logindata").get("soyisim").toString().toUpperCase()}",
                        style: TextStyle(color: FixedColor().color3),
                      );
                    })
              ],
            ),
            centerTitle: true,
            backgroundColor: FixedColor().color1,
            shadowColor: FixedColor().color1,
            leadingWidth: 70,
            // actions: [
            //   Padding(
            //       padding: const EdgeInsets.only(right: 5),
            //       child: StreamBuilder<List<dynamic>>(
            //           stream: listenDocument.duyuruStream(),
            //           builder: (BuildContext context,
            //               AsyncSnapshot<List<dynamic>> snapshot) {
            //             if (snapshot.hasError) {
            //               return const Icon(
            //                 Icons.notifications_none,
            //                 size: 40,
            //               );
            //             }

            //             if (snapshot.connectionState ==
            //                 ConnectionState.waiting) {
            //               return const Icon(
            //                 Icons.notifications_none,
            //                 size: 40,
            //               );
            //             }
            //             List<DuyuruModel>? docSnapList =
            //                 snapshot.data!.cast<DuyuruModel>();
            //             List duyurular =
            //                 Hive.box("logindata").get("duyurular") ?? [];
            //             List duyurular1 = [];
            //             Function eq = const ListEquality().equals;
            //             bool equals = true;
            //             for (var i in docSnapList) {
            //               duyurular1.add(i.id);
            //             }
            //             if (eq(duyurular1, duyurular)) {
            //               equals = true;
            //             } else if (duyurular.length > duyurular1.length) {
            //               equals = true;
            //             } else {
            //               equals = false;
            //             }
            //             return IconButton(
            //               onPressed: () {
            //                 Navigator.push(
            //                     context,
            //                     PageTransition(
            //                         type: PageTransitionType.rightToLeft,
            //                         duration: const Duration(milliseconds: 500),
            //                         child: NotificationPage(
            //                           docSnapList: docSnapList,
            //                           duyurular1: duyurular1,
            //                         )));
            //               },
            //               icon: const Icon(
            //                 Icons.notifications_none,
            //                 size: 40,
            //               ),
            //               color: equals ? FixedColor().color2 : Colors.red,
            //             );
            //           }))
            // ],
            leading:
                const Image(image: AssetImage("images/dhbtLeadinglogo.PNG")),
          ),
          body: SafeArea(child: selectedNotify.showPage()),
          bottomNavigationBar: BottomNavigationBar(
            items: <BottomNavigationBarItem>[
              BottomNavigationBarItem(
                icon: Icon(Icons.home_outlined,
                    color: model.selectedIndex == 0
                        ? FixedColor().color3
                        : FixedColor().color4),
                label: 'ANASAYFA',
              ),
              BottomNavigationBarItem(
                icon: ImageIcon(
                  const AssetImage("images/icons/hapbilgiler.png"),
                  color: model.selectedIndex == 1
                      ? FixedColor().color3
                      : FixedColor().color4,
                ),
                label: 'HAP BİLGİLER',
              ),
              BottomNavigationBarItem(
                icon: Icon(
                  Icons.person_outline,
                  color: model.selectedIndex == 2
                      ? FixedColor().color3
                      : FixedColor().color4,
                ),
                label: "PROFİL ",
              ),
            ],
            currentIndex: model.selectedIndex,
            selectedItemColor: FixedColor().color3,
            unselectedItemColor: FixedColor().color4,
            onTap: selectedNotify.onItemTapped,
          ),
        );
      },
    );
  }
}
