import 'package:flutter/material.dart';

import '../constants/color.dart';

class Score extends StatefulWidget {
  late String titletext;
  late double titlesize;
  late double scoretext;
  late double scoresize;
  Score(
      {Key? key,
      required this.titletext,
      required this.titlesize,
      required this.scoretext,
      required this.scoresize})
      : super(key: key);

  @override
  _ScoreState createState() => _ScoreState();
}

class _ScoreState extends State<Score> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(9.0),
      child: Column(
        children: [
          Text(
            widget.titletext,
            style: TextStyle(
                color: FixedColor().color2, fontSize: widget.titlesize),
          ),
          Text(
            widget.scoretext.toString(),
            style: TextStyle(
                color: FixedColor().color3, fontSize: widget.scoresize),
          )
        ],
      ),
    );
  }
}
