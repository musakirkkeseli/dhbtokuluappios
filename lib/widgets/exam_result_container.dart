//exam resılt  container
//kişi sorularının çekilip ve cevap anahtarının çekilip dopru yanlış değerlendirmesi

import 'package:dhbtokulu/constants/color.dart';
import 'package:dhbtokulu/widgets/exam_result_container_row.dart';
import 'package:flutter/material.dart';

class ExamResultContainer extends StatefulWidget {
  late String text;
  late int dogruCevapSayisi;
  late int yanlisCevapSayisi;
  late int bosCevapSayisi;
  ExamResultContainer(
      {Key? key,
      required this.text,
      required this.dogruCevapSayisi,
      required this.yanlisCevapSayisi,
      required this.bosCevapSayisi})
      : super(key: key);

  @override
  _ExamResultContainerState createState() => _ExamResultContainerState();
}

class _ExamResultContainerState extends State<ExamResultContainer> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          padding: const EdgeInsets.all(16),
          child: Text(
            widget.text,
            style: const TextStyle(fontSize: 20, fontWeight: FontWeight.w900),
          ),
        ),
        Card(
          child: Column(
            children: [
              const Padding(
                padding: EdgeInsets.all(8.0),
                child: CircleAvatar(
                  radius: 90,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  children: [
                    ExamResultContainerRow(
                      text: "Doğru\nSoru\nSayısı",
                      sayi: widget.dogruCevapSayisi,
                      icon: Icon(
                        Icons.check_circle,
                        color: FixedColor().color3,
                      ),
                    ),
                    Divider(
                      color: FixedColor().color2,
                    ),
                    ExamResultContainerRow(
                      text: "Yanlış\nSoru\nSayısı",
                      sayi: widget.yanlisCevapSayisi,
                      icon: Icon(
                        Icons.cancel,
                        color: FixedColor().color2,
                      ),
                    ),
                    Divider(
                      color: FixedColor().color2,
                    ),
                    ExamResultContainerRow(
                      text: "Boş\nSoru\nSayısı",
                      sayi: widget.bosCevapSayisi,
                      icon: const Icon(Icons.error),
                    )
                  ],
                ),
              ),
            ],
          ),
        )
      ],
    );
  }
}
