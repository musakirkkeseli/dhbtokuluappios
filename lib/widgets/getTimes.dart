import 'package:cloud_firestore/cloud_firestore.dart';

class getTimes {
  DateTime getTheTimes() {
    DateTime currentTimeDate = DateTime.now();
    Timestamp myTimeStamp = Timestamp.fromDate(currentTimeDate);
    DateTime myDateTime = myTimeStamp.toDate();
    return myDateTime;
  }
}
