import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:dhbt_okulu/model/slide_item_count.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class SlideItem extends StatelessWidget {
  final int index;
  SlideItem(this.index);
  @override
  Widget build(BuildContext context) {
    final Future<QuerySnapshot> _slideStream = FirebaseFirestore.instance
        .collection('slide')
        .orderBy('sira', descending: false)
        .get();
    return FutureBuilder<QuerySnapshot>(
        future: _slideStream,
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasError) {
            return const Text('Bir şeyler yanlış gitti');
          }

          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(child: CircularProgressIndicator());
          }

          // slider notify işlemleri
          // slideItemNotify.onRefresh(snapshot.data!.docs.toList().length);
          // slideItemNotify.value.itemCount = snapshot.data!.docs.toList().length;
          return TextButton(
            onPressed: () {
              void _launchURL() async =>
                  await launch(snapshot.data!.docs.toList()[index]['url']);
              _launchURL();
            },
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: NetworkImage(
                          snapshot.data!.docs.toList()[index]['fotoUrl']),
                      fit: BoxFit.fill)),
            ),
          );
        });
  }
}
