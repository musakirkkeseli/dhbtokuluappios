import 'package:flutter/material.dart';

import '../constants/color.dart';
import '../model/trial_exam_provider.dart';

class ExamQuesButton extends StatefulWidget {
  late int quesnum;
  late int choicenum;
  late String choicetext;
  ExamQuesButton(
      {Key? key,
      required this.quesnum,
      required this.choicenum,
      required this.choicetext})
      : super(key: key);

  @override
  _ExamQuesButtonState createState() => _ExamQuesButtonState();
}

class _ExamQuesButtonState extends State<ExamQuesButton> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ValueListenableBuilder(
            valueListenable: choiceNotify,
            builder: (BuildContext context, ExamQuesChoiceModel model, child) {
              return ListTile(
                leading: Text(
                  _choice(),
                  style: TextStyle(
                      color: FixedColor().color4,
                      fontSize: 17,
                      fontWeight: FontWeight.bold),
                ),
                title: Text(
                  widget.choicetext,
                  style: TextStyle(
                      color: FixedColor().color4,
                      fontWeight: FontWeight.normal),
                ),
                enabled: true,
                contentPadding: const EdgeInsets.symmetric(horizontal: 20),
                minLeadingWidth: 0,
                horizontalTitleGap: 5,
                selected: model.choiceMap[widget.quesnum] == widget.choicenum,
                selectedTileColor: FixedColor().color3,
                onTap: () {
                  if (model.choiceMap[widget.quesnum] != widget.choicenum) {
                    choiceNotify.addChoice(
                        ExamQuesChoice(widget.quesnum, widget.choicenum));
                    print("tıklanan soru: ${model.choiceMap}");
                  } else {
                    choiceNotify.delChoice(
                        ExamQuesChoice(widget.quesnum, widget.choicenum));
                    print("tıklanan soru: ${model.choiceMap}");
                  }
                },
              );
            }),
        // Divider(height: 0, indent: 20,endIndent: 20,color: FixedColor().color2,)
      ],
    );
    // TextButton.icon(
    //     onPressed: () {},
    //     icon: Text("a)",style: TextStyle(color: FixedColor().color4),),
    //     label: Text("Bende nereden bileyim kardeşim.",style: TextStyle(color: FixedColor().color4,fontWeight: FontWeight.normal),),
    //     );
  }

  _choice() {
    if (widget.choicenum == 1) {
      return "A)";
    } else if (widget.choicenum == 2) {
      return "B)";
    } else if (widget.choicenum == 3) {
      return "C)";
    } else if (widget.choicenum == 4) {
      return "D)";
    } else if (widget.choicenum == 5) {
      return "E) ";
    } else if (widget.choicenum == 6) {
      return "  ";
    }
  }
}
