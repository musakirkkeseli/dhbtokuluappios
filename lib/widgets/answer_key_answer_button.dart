import 'package:flutter/material.dart';

import '../constants/color.dart';
import '../model/trial_exam_provider.dart';

class AnswerButton extends StatefulWidget {
  late int quesnum;
  late int choicenum;
  late String choicetext;
  late int trueValue;
  AnswerButton(
      {Key? key,
      required this.quesnum,
      required this.choicenum,
      required this.choicetext,
      required this.trueValue})
      : super(key: key);

  @override
  _AnswerButtonState createState() => _AnswerButtonState();
}

class _AnswerButtonState extends State<AnswerButton> {
  @override
  Widget build(BuildContext context) {
    late Color choiceColor = Colors.white;
    if (widget.trueValue == 1) {
      choiceColor = FixedColor().color3;
    } else if (widget.trueValue == 0) {
      choiceColor = Colors.white;
    } else if (widget.trueValue == 2) {
      choiceColor = Colors.red;
    } else if (widget.trueValue == 3) {
      choiceColor = Colors.grey;
    }

    return Column(
      children: [
        ValueListenableBuilder(
            valueListenable: choiceNotify,
            builder: (BuildContext context, ExamQuesChoiceModel model, child) {
              return ListTile(
                leading: Text(
                  _choice(),
                  style: TextStyle(
                      color: FixedColor().color4,
                      fontSize: 17,
                      fontWeight: FontWeight.bold),
                ),
                title: Text(
                  widget.choicetext,
                  style: TextStyle(
                      color: FixedColor().color4,
                      fontWeight: FontWeight.normal),
                ),
                enabled: true,
                contentPadding: const EdgeInsets.symmetric(horizontal: 20),
                minLeadingWidth: 0,
                horizontalTitleGap: 5,
                selected: model.choiceMap[widget.quesnum] == widget.choicenum,
                selectedTileColor: FixedColor().color3,
                tileColor: choiceColor,
              );
            }),
        // Divider(height: 0, indent: 20,endIndent: 20,color: FixedColor().color2,)
      ],
    );
    // TextButton.icon(
    //     onPressed: () {},
    //     icon: Text("a)",style: TextStyle(color: FixedColor().color4),),
    //     label: Text("Bende nereden bileyim kardeşim.",style: TextStyle(color: FixedColor().color4,fontWeight: FontWeight.normal),),
    //     );
  }

  _choice() {
    if (widget.choicenum == 1) {
      return "a)";
    } else if (widget.choicenum == 2) {
      return "b)";
    } else if (widget.choicenum == 3) {
      return "c)";
    } else if (widget.choicenum == 4) {
      return "d)";
    } else if (widget.choicenum == 5) {
      return "e)";
    }
  }
}
