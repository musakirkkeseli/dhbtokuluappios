import 'package:flutter/material.dart';

import '../constants/color.dart';
import '../model/auth_user_values_provider.dart';

class RegisterTextFormField extends StatefulWidget {
  RegisterTextFormField({Key? key}) : super(key: key);

  @override
  _RegisterTextFormFieldState createState() => _RegisterTextFormFieldState();
}

class _RegisterTextFormFieldState extends State<RegisterTextFormField> {
  final _emailRegExp = RegExp(
      r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+");
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0),
      child: Center(
        child: Column(
          children: [
            TextFormField(
              maxLength: 30,
              validator: (value) {
                if (value!.isEmpty) {
                  return "Lütfen isminizi giriniz.";
                }
                return null;
              },
              onSaved: (value) {
                authNotify.value.isim = value!;
              },
              onChanged: (value) {
                setState(() {
                  authNotify.value.isim = value;
                });
              },
              decoration: InputDecoration(
                  fillColor: Colors.transparent,
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(
                        color: FixedColor().color3,
                        style: BorderStyle.solid,
                        width: 2),
                  ),
                  labelText: "İsim",
                  labelStyle: TextStyle(color: FixedColor().color2)),
            ),
            TextFormField(
              maxLength: 30,
              validator: (value) {
                if (value!.isEmpty) {
                  return "Lütfen soyisminizi giriniz.";
                }
                return null;
              },
              onSaved: (value) {
                authNotify.value.soyisim = value!;
              },
              onChanged: (value) {
                setState(() {
                  authNotify.value.soyisim = value;
                });
              },
              decoration: InputDecoration(
                  fillColor: Colors.transparent,
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(
                        color: FixedColor().color3,
                        style: BorderStyle.solid,
                        width: 2),
                  ),
                  labelText: "Soyisim",
                  labelStyle: TextStyle(color: FixedColor().color2)),
            ),
            TextFormField(
              maxLength: 30,
              validator: (value) {
                if (value!.isEmpty) {
                  return "Lütfen bir e-posta adresi giriniz.";
                } else if (!_emailRegExp.hasMatch(value)) {
                  return 'Geçersiz e-posta adresi!';
                }
                return null;
              },
              onSaved: (value) {
                authNotify.value.eposta = value!;
              },
              onChanged: (value) {
                setState(() {
                  authNotify.value.eposta = value;
                });
              },
              keyboardType: TextInputType.emailAddress,
              decoration: InputDecoration(
                  fillColor: Colors.transparent,
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(
                        color: FixedColor().color3,
                        style: BorderStyle.solid,
                        width: 2),
                  ),
                  labelText: "E-Posta",
                  labelStyle: TextStyle(color: FixedColor().color2)),
            ),
            TextFormField(
              maxLength: 30,
              validator: (value) {
                if (value!.isEmpty) {
                  return 'Lütfen şifrenizi giriniz.';
                } else if (value.toString().length < 6) {
                  return 'Şifre 6 karakterden az olamaz.';
                }
                return null;
              },
              onSaved: (value) {
                authNotify.value.sifre = value!;
              },
              onChanged: (value) {
                setState(() {
                  authNotify.value.sifre = value;
                });
              },
              obscureText: true,
              decoration: InputDecoration(
                  fillColor: Colors.transparent,
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(
                        color: FixedColor().color3,
                        style: BorderStyle.solid,
                        width: 2),
                  ),
                  labelText: "Sifre",
                  labelStyle: TextStyle(color: FixedColor().color2)),
            ),
            TextFormField(
              maxLength: 30,
              validator: (value) {
                if (value != authNotify.value.sifre) {
                  return 'Şifrenizi doğru şekilde tekrar edin.';
                }
                return null;
              },
              onChanged: (value) {},
              obscureText: true,
              decoration: InputDecoration(
                  fillColor: Colors.transparent,
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(
                        color: FixedColor().color3,
                        style: BorderStyle.solid,
                        width: 2),
                  ),
                  labelText: "Sifre Tekrardan",
                  labelStyle: TextStyle(color: FixedColor().color2)),
            ),
            // TextFormField(
            //   maxLength: 11,
            //   validator: (value) {
            //     if (value!.isEmpty) {
            //       return 'Lütfen şahsınıza ait telefon numarasını giriniz.';
            //     } else if (value.toString().length != 11) {
            //       return 'Girdiğiniz numara başındaki sıfırla beraber 11 karakter olmalı.';
            //     }
            //     return null;
            //   },
            //   onSaved: (value) {
            //     authNotify.value.gsm = value!;
            //   },
            //   onChanged: (value) {
            //     setState(() {
            //       authNotify.value.gsm = value;
            //     });
            //   },
            //   keyboardType: TextInputType.phone,
            //   decoration: InputDecoration(
            //       fillColor: Colors.transparent,
            //       focusedBorder: UnderlineInputBorder(
            //         borderSide: BorderSide(
            //             color: FixedColor().color3,
            //             style: BorderStyle.solid,
            //             width: 2),
            //       ),
            //       labelText: "Telefon Numarası",
            //       labelStyle: TextStyle(color: FixedColor().color2)),
            // ),
          ],
        ),
      ),
    );
  }
}
