import 'package:flutter/material.dart';

import '../constants/color.dart';
import '../model/answer_key_filter_provider.dart';

class FilterButton extends StatelessWidget {
  late int rownum;
  late Icon icon;
  late int filterofnum;
  FilterButton(
      {Key? key,
      required this.icon,
      required this.rownum,
      required this.filterofnum})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder(
        valueListenable: answerChoiceNotify,
        builder: (BuildContext context, AnswerKeyFilterChoice model, child) {
          return Padding(
            padding: const EdgeInsets.all(3.0),
            child: Stack(
              children: [
                OutlinedButton.icon(
                  onPressed: () {
                    if (model.selectedIndex == rownum) {
                      answerChoiceNotify.onItemTapped(0);
                    } else {
                      answerChoiceNotify.onItemTapped(rownum);
                    }
                  },
                  icon: icon,
                  label: Text(
                    _labeltext(),
                    style: TextStyle(color: FixedColor().color3, fontSize: 10),
                  ),
                  style: OutlinedButton.styleFrom(
                      primary: Colors.white,
                      side: model.selectedIndex == rownum
                          ? BorderSide(color: FixedColor().color3, width: 3)
                          : null,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(50)),
                      fixedSize: Size(MediaQuery.of(context).size.width * 0.3,
                          MediaQuery.of(context).size.height * 0.05)),
                ),
                Positioned(
                  left: MediaQuery.of(context).size.width * 0.002,
                  child: CircleAvatar(
                    child: Text(
                      "$filterofnum",
                      style: TextStyle(color: FixedColor().color1),
                    ),
                    backgroundColor: FixedColor().color3,
                    maxRadius: 12,
                  ),
                )
              ],
            ),
          );
        });
  }

  _labeltext() {
    if (rownum == 1) {
      return "Doğru Cevaplar";
    } else if (rownum == 2) {
      return "Yalnış Cevaplar";
    } else if (rownum == 3) {
      return "Boş Cevaplar";
    }
  }
}
