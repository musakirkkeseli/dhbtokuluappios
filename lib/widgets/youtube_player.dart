import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';
// import 'package:hive/hive.dart';
// import 'package:page_transition/page_transition.dart';
// import 'package:provider/provider.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

import '../constants/color.dart';
// import '../model/duyuru_model.dart';
// import '../screen/notification_page.dart';
// import '../services/firebase_database.dart';

class YoutubePlayerWidget extends StatefulWidget {
  late String videoUrl;
  YoutubePlayerWidget({Key? key, required this.videoUrl}) : super(key: key);

  @override
  _YoutubePlayerWidgetState createState() => _YoutubePlayerWidgetState();
}

class _YoutubePlayerWidgetState extends State<YoutubePlayerWidget> {
  ytUrl() {
    try {
      String? videoID = YoutubePlayer.convertUrlToId(widget.videoUrl);
      return videoID;
    } on Exception catch (exception) {
      print(exception);
      return "https://www.youtube.com/watch?v=_zhLRIq-9bs&t=1725s";
    } catch (error) {
      print(error);
      return "";
    }
  }

  @override
  Widget build(BuildContext context) {
    // final listenDocument =
    //     Provider.of<FirebaseDatabaseService>(context, listen: false);
    return Scaffold(
      appBar: AppBar(
        title: Column(
          children: [
            Text(
              "Merhaba",
              style: TextStyle(color: FixedColor().color2),
            ),
            Text(
              Hive.box("logindata").get("isim") == null
                  ? ""
                  : "${Hive.box("logindata").get("isim").toString().toUpperCase()} ${Hive.box("logindata").get("soyisim").toString().toUpperCase()}",
              style: TextStyle(color: FixedColor().color3),
            )
          ],
        ),
        centerTitle: true,
        backgroundColor: FixedColor().color1,
        shadowColor: FixedColor().color1,
        iconTheme: IconThemeData(
          color: FixedColor().color3,
        ),
        // actions: [
        //   Padding(
        //       padding: const EdgeInsets.only(right: 5),
        //       child: StreamBuilder<List<dynamic>>(
        //           stream: listenDocument.duyuruStream(),
        //           builder: (BuildContext context,
        //               AsyncSnapshot<List<dynamic>> snapshot) {
        //             if (snapshot.hasError) {
        //               return const Icon(
        //                 Icons.notifications_none,
        //                 size: 40,
        //               );
        //             }

        //             if (snapshot.connectionState == ConnectionState.waiting) {
        //               return const Icon(
        //                 Icons.notifications_none,
        //                 size: 40,
        //               );
        //             }
        //             List<DuyuruModel>? docSnapList =
        //                 snapshot.data!.cast<DuyuruModel>();
        //             List? duyurular = Hive.box("logindata").get("duyurular");
        //             List duyurular1 = [];
        //             Function eq = const ListEquality().equals;
        //             bool equals = true;
        //             for (var i in docSnapList) {
        //               duyurular1.add(i.id);
        //             }
        //             if (eq(duyurular1, duyurular)) {
        //               equals = true;
        //             } else if (duyurular!.length > duyurular1.length) {
        //               equals = true;
        //             } else {
        //               equals = false;
        //             }
        //             return IconButton(
        //               onPressed: () {
        //                 Navigator.push(
        //                     context,
        //                     PageTransition(
        //                         type: PageTransitionType.rightToLeft,
        //                         duration: const Duration(milliseconds: 500),
        //                         child: NotificationPage(
        //                           docSnapList: docSnapList,
        //                           duyurular1: duyurular1,
        //                         )));
        //               },
        //               icon: const Icon(
        //                 Icons.notifications_none,
        //                 size: 40,
        //               ),
        //               color: equals ? FixedColor().color2 : Colors.red,
        //             );
        //           }))
        // ],
      ),
      body: SafeArea(
        child: ytUrl() == null
            ? const Center(child: Text("Seminer videosu bulunamadı."))
            : YoutubePlayer(
                controller: YoutubePlayerController(
                  initialVideoId: ytUrl(),
                  flags: const YoutubePlayerFlags(
                    hideControls: false,
                    controlsVisibleAtStart: true,
                    autoPlay: true,
                    mute: false,
                  ),
                ),
                showVideoProgressIndicator: true,
                progressIndicatorColor: FixedColor().color3,
              ),
      ),
    );
  }
}
