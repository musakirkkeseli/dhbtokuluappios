import 'package:flutter/material.dart';

final authNotify = ChoiceNotify(AuthUserValuesModel());

class ChoiceNotify extends ValueNotifier<AuthUserValuesModel> {
  ChoiceNotify(AuthUserValuesModel value) : super(value);

  void changeAuthValues(String email, String password, String ad, String soyad,
      String tel, String docid) {
    value.eposta = email;
    value.sifre = password;
    value.isim = ad;
    value.soyisim = soyad;
    value.gsm = tel;
    value.docID = docid;
    notifyListeners();
  }

  void deleteAuthValues() {
    value.eposta = " ";
    value.sifre = " ";
    value.isim = " ";
    value.soyisim = " ";
    value.gsm = " ";
    value.docID = " ";
    notifyListeners();
  }
}

class AuthUserValuesModel {
  late String eposta = "";
  late String sifre = "";
  late String isim = "";
  late String soyisim = "";
  late String gsm = "";
  late String docID = "";
  late List<String> kisiCevap;
  late List<String> cevapAnahtari;
}
