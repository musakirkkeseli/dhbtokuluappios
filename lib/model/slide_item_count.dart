import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';

final slideItemNotify = ChoiceNotify(StaticSlideItemCount());

class ChoiceNotify extends ValueNotifier<StaticSlideItemCount> {
  ChoiceNotify(StaticSlideItemCount value) : super(value);
  void onRefresh(int index) {
    value.itemCount = index;
    notifyListeners();
  }

  slideItemStream() async {
    Stream ali = FirebaseFirestore.instance.collection('slide').snapshots();
    value.itemCount = await ali.length;
    return await ali;
  }
}

class StaticSlideItemCount {
  late int itemCount = 2;
}
