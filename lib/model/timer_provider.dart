import 'package:flutter/material.dart';

final timeNotify = TimeNotify(FinishTime());

class TimeNotify extends ValueNotifier<FinishTime> {
  TimeNotify(FinishTime value) : super(value);

  void onfinishTime(tarih) {
    DateTime dateTarih = tarih.toDate();
    DateTime nowTime = DateTime.now();
    Duration difTime = dateTarih.difference(nowTime);
    value.gun = difTime.inDays;
    value.saat = difTime.inHours % 24;
    value.dakika = difTime.inMinutes % 60;
    value.saniye = difTime.inSeconds % 60;
    notifyListeners();
  }
}

class FinishTime {
  late int gun = 0;
  late int saat = 0;
  late int dakika = 0;
  late int saniye = 0;
}
