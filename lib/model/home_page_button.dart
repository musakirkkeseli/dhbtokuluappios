import 'package:flutter/material.dart';

class HomeButtom {
  final AssetImage images;
  final String text;

  HomeButtom({required this.images, required this.text});
}

final homebuttonList = [
  HomeButtom(
      images: const AssetImage("images/icons/hapbilgiler.png"),
      text: "Hap Bilgiler"),
  HomeButtom(
      images: const AssetImage("images/icons/denemeSinavlari.png"),
      text: "Deneme \n Sınavları"),
  HomeButtom(
      images: const AssetImage("images/icons/puanHesapla.png"),
      text: "Puan \n Hesaplama"),
  HomeButtom(
      images: const AssetImage("images/icons/cekilis.png"), text: "Çekiliş"),
  HomeButtom(
      images: const AssetImage("images/instagram.png"), text: "İnstagram"),
  HomeButtom(
      images: const AssetImage("images/icons/seminar.png"), text: "Seminer"),
];
