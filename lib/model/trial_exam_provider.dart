import 'package:flutter/material.dart';

final choiceNotify = ChoiceNotify(ExamQuesChoiceModel());

class ChoiceNotify extends ValueNotifier<ExamQuesChoiceModel> {
  ChoiceNotify(ExamQuesChoiceModel value) : super(value);

  void addChoice(ExamQuesChoice examQuesChoice) {
    value.choiceMap[examQuesChoice.quesnum] = examQuesChoice.choicenum;
    notifyListeners();
  }

  void delChoice(ExamQuesChoice examQuesChoice) {
    value.choiceMap.remove(examQuesChoice.quesnum);
    notifyListeners();
  }

  void clearChoice() {
    value.choiceMap.clear();
    notifyListeners();
  }
}

class ExamQuesChoiceModel {
  late Map<int, int> choiceMap = {};
  late String examName = "dhbt";
}

class ExamQuesChoice {
  late int quesnum;
  late int choicenum;

  ExamQuesChoice(this.quesnum, this.choicenum);
}
