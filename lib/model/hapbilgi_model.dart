class HapBilgiModel {
  final String id;
  final String imageUrl;

  HapBilgiModel({
    required this.id,
    required this.imageUrl,
  });

  factory HapBilgiModel.fromMap(Map<String, dynamic> data, String documentId) {
    // if (data == null) {
    //   return null;
    // }

    String imageUrl = data['url'];

    return HapBilgiModel(id: documentId, imageUrl: imageUrl);
  }

  Map<String, dynamic> toMap() {
    return {
      'imageUrl': imageUrl,
    };
  }
}
