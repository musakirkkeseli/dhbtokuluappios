import 'package:flutter/material.dart';

import '../screen/hap_bilgiler.dart';
import '../screen/home_page.dart';
import '../screen/profil_page.dart';

final selectedNotify = SelectedNotify(ItemTapped());

class SelectedNotify extends ValueNotifier<ItemTapped> {
  SelectedNotify(ItemTapped value) : super(value);

  void onItemTapped(int index) {
    value.selectedIndex = index;
    notifyListeners();
  }

  Widget showPage() {
    if (value.selectedIndex == 0) {
      return const MyHomePage();
    } else if (value.selectedIndex == 1) {
      return const HapBilgiler();
    }
    return Profil();
  }
}

class ItemTapped {
  late int selectedIndex = 0;
}
